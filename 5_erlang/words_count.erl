-module(words_count).
-export([count_words/1]).

list_length([]) -> 0;
list_length([_ | Tail]) ->  1 + list_length(Tail).

count_words(Text) ->  R = re:split(Text, " "), list_length(R).
