-module(k_v).
-export([k_v/2]).

k_v(List, Key) -> lists:nth(1, [{V} || {K, V} <- List, K == Key]).

% List = [{erlang, "a functional language"}, {ruby, "an OO language"}]
% k_v:k_v(List, erlang).