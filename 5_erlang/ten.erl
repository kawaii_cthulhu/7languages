-module(ten).
-export([count/1]).

count(10) -> 10;
count(N) -> count(N + 1).