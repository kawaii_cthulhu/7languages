-module(doctor_watcher).
-export([d_loop/0, w_loop/0]).

d_loop() ->
    process_flag(trap_exit, true),
    receive
        new ->
            io:format("Creating and monitoring process.~n"),
            register(watcher, spawn_link(fun w_loop/0)),
            d_loop();

        {'EXIT', From, Reason} ->
            io:format("The shooter ~p died with reason ~p.", [From, Reason]),
                        io:format(" Restarting. ~n"),
            self() ! new,
            d_loop()
        end.


w_loop() ->
  process_flag(trap_exit, true),
  receive
    new ->
      io:format("Creating and monitoring new Doctor process.~n"),
      register(doctor, spawn_link(fun d_loop/0)),
      doctor ! new,
      w_loop();
    {'EXIT', From, Reason} ->
      io:format("The doctor ~p died with reason ~p.~n", [From, Reason]),
      io:format("Restarting... ~n"),
      self() ! new,
      w_loop()
end.


% Doctor = spawn(fun self_doctor:loop/0).
% Doctor ! new.
% idk if it works