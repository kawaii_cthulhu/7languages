-module(error).
-export([print_msg/1]).

print_msg(success) -> io:fwrite("success~n");
print_msg({error, Message}) -> io:fwrite("error: ~s~n", [Message]).