-module(total_price).
-export([add_total/1]).

add_total(List) -> [{Item, Quantity * Price} || {Item, Quantity, Price} <- List].

% total_price:add_total([{milk, 4, 0.5}, {water, 8, 1.2}]).
