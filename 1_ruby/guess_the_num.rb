n = rand(10)
puts 'Guess the number [0, 10):'
guess = gets.to_i
guessed = false
while not guessed
  if guess == n
    puts "Yes, it's #{n}!"
    guessed = true
  else
    if guess > n
      puts 'Too high!'
    else
      puts 'Too low!'
    end
    guess = gets.to_i
  end
end
