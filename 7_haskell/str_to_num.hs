module StrToNum where
    valid_chars = ['0'..'9'] ++ ['.']
    filter_num s = filter (`elem` valid_chars) s
    str_to_num s = read (filter_num s) :: Float
