module Colors where
    color_pairs = [(a, b) | a <- colors, b <- colors, a < b] where colors = ["black", "white", "blue", "yellow", "red"]

