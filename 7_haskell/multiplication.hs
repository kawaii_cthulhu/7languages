module Mult where
    multiplication_table = [(a, b, a * b) | a <- r, b <- r] where r = [1..12]