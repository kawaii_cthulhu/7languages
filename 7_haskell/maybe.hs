module Maybe where
    paragraph XmlDoc -> XmlDoc
    body XmlDoc -> XmlDoc
    html XmlDoc -> XmlDoc

    paragraph body (html doc)

    case (html doc) of
      Nothing -> Nothing
      Just x -> case body x of
                  Nothing -> Nothing
                  Just y -> paragraph 2 y

    data Maybe a = Nothing | Just a

    instance Monad Maybe  where
        return = Just
        Nothing >>= f = Nothing
        (Just x) >>= f = f x

    Just someWebPage >>= html >>= body >>= paragraph >>= return
