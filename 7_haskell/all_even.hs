module Even where
    allEven :: [Integer] -> [Integer]
    allEven [] = []
    allEven (h:t) = if even h then h:allEven t else allEven t

    allEvenF :: [Integer] -> [Integer]
    allEvenF l = filter even l

    allEvenLC :: [Integer] -> [Integer]
    allEvenLC l = [x | x <- l, even x]