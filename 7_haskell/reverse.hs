module Reverse where
    reverse_list [] = []
    reverse_list (h:t) = reverse_list(t) ++ [h]
