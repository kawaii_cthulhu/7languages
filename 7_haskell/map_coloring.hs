module Map where
    colors = ["green", "blue", "red"]
    color = head [[("Alabama", c1), ("Florida", c2), ("Georgia", c3),
                  ("Mississippi", c4), ("Tennessee", c5)]
                  | c1 <- colors, c2 <- colors, c3 <- colors, c4 <- colors, c5 <- colors,
                    notElem c1 [c2, c3, c4, c5], notElem c3 [c2, c5], c4 /= c5]

--different(Mississippi, Tennessee),
--different(Alabama, Tennessee),
--different(Alabama, Mississippi),
--different(Alabama, Georgia),
--different(Alabama, Florida),
--different(Georgia, Florida),
--different(Georgia, Tennessee).