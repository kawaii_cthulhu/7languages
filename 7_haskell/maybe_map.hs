--- https://blog.wakatta.jp/blog/2011/11/19/seven-languages-in-seven-weeks-haskell-day-3/
module Maybe where
    my_lookup k = Nothing
    my_lookup k ((k, value):rest)
      | key == k  = Just value
      | otherwise = lookup key rest

    testData = [(1, []), (2, [("a", [("i", "tada!")]), ("b", [("j", "nope")])]), (3, [("c", [("k", "tada!")])])]
    my_lookup 2 testData >>= my_lookup "a" >>= my_lookup "i"
    my_lookup 2 testData >>= my_lookup "b" >>= my_lookup "i"