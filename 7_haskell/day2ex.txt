Find:
• Functions that you can use on lists, strings, or tuples
https://wiki.haskell.org/Cookbook/Lists_and_strings
http://www.toves.org/books/hslist/
• A way to sort lists
https://stackoverflow.com/questions/19082953/how-to-sort-a-list-in-haskell-in-command-line-ghci
http://hackage.haskell.org/package/base-4.12.0.0/docs/Data-List.html

Do:
• Write a sort that takes a list and returns a sorted list.
import Data.List (sort)
sort_list l = sort l
• Write a sort that takes a list and a function that compares its two arguments and then returns a sorted list.
import Data.List (sortOn)
sort_list_by f l = sortOn f l
• Write a Haskell function to convert a string to a number. The string should be in the form of $2,345,678.99 and can possibly have leading zeros.
str_to_num.hs
• Write a function that takes an argument x and returns a lazy sequence that has every third number, starting with x. Then, write a function that includes every fifth number, beginning with y. Combine these functions through composition to return every eighth number, beginning with x + y.
range3 start = start:(range3 (start + 3))
range5 start = start:(range5 (start + 5))
take 10 (zipWith (+) (range3 4) (range5 6))
• Use a partially applied function to define a function that will return half of a number and another that will append \n to the end of any string.
half.hs

Here are some more demanding problems if you’re looking for something even more interesting:
• Write a function to determine the greatest common denominator of two integers.
https://lettier.github.io/posts/2016-04-22-fibonacci-lcm-and-gcd-in-haskell.html
• Create a lazy sequence of prime numbers.
https://stackoverflow.com/questions/3596502/lazy-list-of-prime-numbers
• Break a long string into individual lines at proper word boundaries.
https://www.reddit.com/r/haskell/comments/3msu8f/separate_words_based_on_nonletters/
• Add line numbers to the previous exercise.
• To the above exercise, add functions to left, right, and fully justify the text with spaces (making both margins straight).