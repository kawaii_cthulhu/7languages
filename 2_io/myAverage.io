List myAverage := method(if(self size == 0,
                            return nil,
                            num := true;
                            self foreach(i, v, if(v type != 1 type, num = false));
                            if(num,
                               return self average,
                               Exception raise("Can't count average of a list containing non-Numeric elements."))))
l := list()
l myAverage # nil
l = list(1, 2, 3)
l myAverage # 2
l = list(1, "2")
l myAverage # Exception: Can't count average of a list containing non-Numeric elements.