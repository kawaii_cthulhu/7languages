Builder := Object clone

OperatorTable addAssignOperator(":" , "atPutNumber")
curlyBrackets := method(
  r := Map clone
  call message arguments foreach(arg,
       r doString(arg asString)
       )
  r
)
Map atPutNumber := method(
  self atPut(
       call evalArgAt(0) asMutable removePrefix("\"") removeSuffix(" \"" ),
       call evalArgAt(1))
)

Builder indent := 0
Builder forward := method(
  write("  " repeated(self indent), "<" , call message name)
  self indent = self indent + 1;
  i := 0;
  call message arguments foreach(
        arg,
        content := self doMessage(arg);
        if(content type == "Sequence" , writeln("  " repeated(self indent), content))
        if(content type == "Map",
           str := ""; content foreach(k, v, str = "#{str} #{k}=\"#{v}\"" interpolate);
           writeln("  " repeated(self indent), str, ">")))
  self indent = self indent - 1;
  writeln("  " repeated(self indent), "</" , call message name, ">"))

Builder ul(
        li("Io" ),
        li("Lua" ),
        li("JavaScript" ))

Builder book({"author": "Tate"})