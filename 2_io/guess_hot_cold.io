n := Random value(1, 100) round
"Guess the number 1-100:" println
dif := 100
new_dif := 100
for(i, 1, 10,
    x := File standardInput readLine asNumber
    if(x == n, "Correct!" println; break,
       new_dif = (x - n) abs;
       new_dif println;
       if(i > 1,
          if(new_dif > dif, "Colder!" println, "Hotter!" println));
       dif = new_dif)
)
"It was " println
n println

