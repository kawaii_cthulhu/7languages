n := Random value(1, 100) round
"Guess the number 1-100:" println
for(i, 1, 10,
    x := File standardInput readLine asNumber
    if(x == n, "Correct!" println; break, if(x < n, "Too low!" println, "Too high!" println))
)
"It was " println
n println