Matrix := List clone
Matrix dim := method(x, y,
                     m := Matrix();
                     for(i, 1, y,
                         row := list(nil);
                         row setSize(x);
                         m push(row));
                     return m)
Matrix set := method(x, y, value,
                     self at(y) atPut(x, value))
Matrix get := method(x, y,
                     return self at(y) at(x))

List transpose := method(t := list();
                         x := self size;
                         y := self at(0) size;
                         for(i, 0, y - 1,
                             row := list();
                             row setSize(x);
                             for(j, 0, x - 1,
                                 row atPut(j, self at(j) at(i)));
                             t push(row);
                             );
                         return t)

m := Matrix dim(2,3)
m set(0, 0, 1)
m set(1, 0, 2)
m set(0, 1, 3)
m set(1, 1, 4)
m set(0, 2, 5)
m set(1, 2, 6)
m println
m get(0, 0) println
m get(1, 2) println

m transpose println

f := File with("matrix.txt")
f openForUpdating
f write(m serialized)
f close

f openForReading
m = f contents
f close
m = Matrix(doString(m))
m println
