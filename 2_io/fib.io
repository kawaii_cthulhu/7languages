fib := method(n,
    if(n < 3, return 1, return fib(n - 1) + fib(n - 2))
)
fib := method(n,
    if(n < 2, return n)
    s := 0; i := 1; n1 := 0; n2 := 1
    while(i < n, s = n1 + n2; n1 = n2; n2 = s; i = i + 1)
    return n2
)

fib(1) println
fib(2) println
fib(3) println
fib(4) println
fib(5) println
fib(6) println
fib(7) println
fib(8) println
fib(10) println