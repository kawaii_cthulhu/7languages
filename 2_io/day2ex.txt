1. A Fibonacci sequence starts with two 1s. Each subsequent number is the sum of the two numbers that came before: 1, 1, 2, 3, 5, 8, 13, 21, and so on. Write a program to find the nth Fibonacci number. fib(1) is 1, and fib(4) is 3. As a bonus, solve the problem with recursion and with loops.
fib.io

2. How would you change / to return 0 if the denominator is zero?
div := Number getSlot("/")
Number / := method(x, if(x == 0, 0, div(x)))

3. Write a program to add up all of the numbers in a two-dimensional array.
matrix_sum := method(list, return l flatten sum)
l := list(list(1, 2, 3), list(4, 5))
l matrix_sum

4. Add a slot called myAverage to a list that computes the average of
all the numbers in a list. What happens if there are no numbers
in a list? (Bonus: Raise an Io exception if any item in the list is not
a number.)
myAverage.io

5. Write a prototype for a two-dimensional list. The dim(x, y) method should allocate a list of y lists that are x elements long. set(x, y, value) should set a value, and get(x, y) should return that value.
matrix.io

6. Bonus: Write a transpose method so that (new_matrix get(y, x)) == matrix get(x, y) on the original list.
matrix.io

7. Write the matrix to a file, and read a matrix from a file.
matrix.io

8. Write a program that gives you ten tries to guess a random number from 1–100. If you would like, give a hint of “hotter” or “colder” after the first guess.
guess.io, guess_hot_cold.io
