(defn create
  []
  (atom [0]))
(defn add
  [accounts]
  (swap! accounts conj 0))
(defn debit
  [accounts num amount]
  (swap! accounts assoc num (+ (@accounts num) amount)))
(defn credit
  [accounts num amount]
  (swap! accounts assoc num (- (@accounts num) amount)))
(def ac (create))
(debit ac 0 2)
(credit ac 0 1)
(add ac)
(debit ac 1 5)
(println (str "Accounts: " @ac))
