(def barber (ref []))
(def waiting (ref []))
(def cuts (ref 0))

(declare cut get-next)

(defn cut []
  (do
    (Thread/sleep 20)
    (dosync
      (ref-set barber [])
      (alter cuts + 1)
      (get-next))))

(defn get-next []
  (if (not (empty? @waiting))
    (do
      (dosync
        (let
          [next-customer (first @waiting)]
          (ref-set barber [next-customer])
          (alter waiting rest)))
    (cut))))

(defn enter [customer]
  (dosync
    (if (empty? @barber)
      (do
        (ref-set barber [customer])
        (future-call cut))
      (if (< (count @waiting) 3)
        (alter waiting concat [customer])))))

(def send-customer
  (future
    (while true
      (do
        (Thread/sleep (+ 10 (rand-int 20)))
        (enter :cust)))))

(Thread/sleep 10000)
(println @cuts)
(shutdown-agents)