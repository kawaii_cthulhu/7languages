import akka.actor._
import akka.actor.ActorDSL._

case object Poke
case object Feeds

class Kid() extends ActWithStash {
  override def act() {
    loop {
      react {
        case Poke => {
          println("Ow...")
          println("Quit it...")
        }
        case Feed => {
          println("Gurgle...")
          println("Burp...")
        }
      }
    }
  }
}

val bart = ActorDSL.actor(new Kid()).start
val lisa = ActorDSL.actor(new Kid()).start
println("Ready to poke and feed...")
bart ! Poke
lisa ! Poke
bart ! Feed
lisa ! Feed