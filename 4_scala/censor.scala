import scala.collection.mutable.HashMap
import scala.io.Source

//val curses = Map("Shoot" -> "Pucky", "Darn" -> "Beans")

var curses = new HashMap[String, String]
val bufferedSource = Source.fromFile("curses.txt")
for (line <- bufferedSource.getLines) {
    var splitted = line.split(" ")
    curses += splitted(0) -> splitted(1)
}

bufferedSource.close

class Text(val text: String)

trait Censor {
  def censor():String = {
    var res = text.text
    for ((curse, repl) <- curses) res = res.replace(curse, repl)
    return res
  }
}

class CensoredText(override val text:String) extends Text(text) with Censor

val text = new CensoredText("Shoot Darn")
println(text.censor)