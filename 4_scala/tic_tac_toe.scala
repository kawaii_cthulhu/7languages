//take a tic-tac-toe board with X, O, and blank
//characters and detect the winner or whether there is a tie or no winner yet

val results = Map(-2 -> "No winner yet", -1 -> "It's a tie!",
                  0 -> "O wins", 1 -> "X wins")

def check_list(list:List[Char]):Int = {
    if (list.forall(el => el == 'X')) {return 1}
    if (list.forall(el => el == 'O')) {return 0}
    return -1
}


class TicTacToe(val board: List[List[Char]]) {
  def check(): Int = {
    // check rows
    for(row <- board) {
        var res = check_list(row)
        if (res >= 0) {println(results(res)); return res}
    }
    // check columns
    for (i <- (0 to 2)) {
        val col = List(board(0)(i), board(1)(i), board(2)(i))
        var res = check_list(col)
        if (res >= 0) {println(results(res)); return res}
    }
    // check diagonals
    val d1 = List(board(0)(0), board(1)(1), board(2)(2))
    val d2 = List(board(2)(0), board(1)(1), board(0)(2))
    var res = check_list(d1)
    if (res >= 0) {println(results(res)); return res}
    res = check_list(d2)
    if (res >= 0) {println(results(res)); return res}
    // check for dots
    board.foreach{row =>
                  if (row.contains('.')) {res = -2; println(results(res)); return res}}
    res = -1
    println(results(res))
    return
  }
}

def test() {
    var board = List(List('O', 'O', 'O'),
                     List('.', 'X', 'X'),
                     List('.', '.', '.'))
    var t = new TicTacToe(board)
    t.check  // O

    board = List(List('X', 'O', 'O'),
                 List('O', 'X', 'X'),
                 List('O', 'X', 'O'))
    t = new TicTacToe(board)
    t.check // Tie

    board = List(List('X', 'O', 'O'),
                 List('.', 'X', 'X'),
                 List('.', '.', 'X'))
    t = new TicTacToe(board)
    t.check // X

    board = List(List('X', 'O', 'O'),
                 List('.', 'X', 'X'),
                 List('.', '.', 'O'))
    t = new TicTacToe(board)
    t.check // No winner yet
}

test()