import scala.io._

object PageLoader {
    def getLinksCount(url : String) = Source.fromURL(url).mkString.sliding(4).count(window => window == "</a>")
}
val urls = List("http://www.amazon.com/",
                "http://www.twitter.com/",
                "http://www.google.com/",
                "http://www.cnn.com/" )

def getPageSizeSequentially() = {
    for(url <- urls) {
        println("Size for " + url + ": " + PageLoader.getLinksCount(url));
        println(Source.fromURL(url).mkString.length)
    }
}

getPageSizeSequentially
