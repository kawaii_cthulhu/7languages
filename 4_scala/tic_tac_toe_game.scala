//take a tic-tac-toe board with X, O, and blank
//characters and detect the winner or whether there is a tie or no winner yet

val results = Map(-2 -> "No winner yet", -1 -> "It's a tie!",
                  0 -> "O wins", 1 -> "X wins")

def check_list(list:List[Char]):Int = {
    if (list.forall(el => el == 'X')) {return 1}
    if (list.forall(el => el == 'O')) {return 0}
    return -1
}


class TicTacToe(var board: List[List[Char]]) {
  def check(): Int = {
    // check rows
    for(row <- board) {
        var res = check_list(row)
        if (res >= 0) {return res}
    }
    // check columns
    for (i <- (0 to 2)) {
        val col = List(board(0)(i), board(1)(i), board(2)(i))
        var res = check_list(col)
        if (res >= 0) {return res}
    }
    // check diagonals
    val d1 = List(board(0)(0), board(1)(1), board(2)(2))
    val d2 = List(board(2)(0), board(1)(1), board(0)(2))
    var res = check_list(d1)
    if (res >= 0) {return res}
    res = check_list(d2)
    if (res >= 0) {return res}
    // check for dots
    board.foreach{row => if (row.contains('.')) {return -2}}
    return -1
  }

  def play() {
    var player_x = true
    while (true) {
        var player = if (player_x) 'X' else 'O'
        println(s"Player $player, what's your next move? (e.g. 0 1)")
        var x_y = scala.io.StdIn.readLine().split(" ").map(_.toInt)
        var x = x_y(0)
        var y = x_y(1)
        board = board.updated(x, board(x).updated(y, player))
        var res = check()
        if (res >= 0) {println(results(res)); return}
        player_x = !player_x
        println()
        board.foreach {row => println(row mkString " ")}
    }
  }
}


val board = List(List('.', '.', '.'),
             List('.', '.', '.'),
             List('.', '.', '.'))
val t = new TicTacToe(board)
t.play