plays(alexi, guitar).
plays(henkka, bass).
plays(jaska, drums).
plays(janne, keyboards).
plays(roope, guitar).
plays(alexander, guitar).

plays(elias, guitar).
plays(henrik, keyboards).
plays(tommy, drums).
plays(pasi, bass).

genre(mdm, alexi).
genre(mdm, henkka).
genre(mdm, jaska).
genre(mdm, janne).
genre(mdm, roope).
genre(mdm, alexander).

genre(power, elias).
genre(power, henrik).
genre(power, tommy).
genre(power, pasi).