/*reverse1([], []).
reverse([Head|[]], [[]|Head]).
reverse([Head1|[Head2|[]]], [[[]|Head2]|Head1]).
reverse([Head1|[Head2|[Head3|[]]]], [Head3, Head2,Head1]).*/

reverse([], Acc, Acc).
reverse([Head|Tail], Acc, R) :- reverse(Tail, [Head|Acc], R).