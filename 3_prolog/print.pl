 descendant(athena, poseidon).

 print_solution :-
 (  descendant(athena, poseidon)
 -> Z = true
 ;  Z = false
 ),
 write('Athena is a descendant of Poseidon: '), write(Z), nl.